<?php

use App\Http\Controllers\Admin\{
    UserController,
    ProductController,
    CategoryController,
    DashboardController
};
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function (){
    Route::any('users/search', [UserController::class, 'search'])->name('users.search');
    Route::resource('users', UserController::class)->names('users');

    Route::any('products/search', [ProductController::class, 'search'])->name('products.search');
    Route::resource('products', ProductController::class)->names('products');

    Route::any('/categories/search', [CategoryController::class, 'search'])->name('categories.search');
    Route::resource('/categories', CategoryController::class)->names('categories');

    Route::get('/', [DashboardController::class, 'index'])->name('admin');
});

Auth::routes(['register' => false]);

Route::get('/', function () {
    return view('welcome');
});
