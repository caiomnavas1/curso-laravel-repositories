<?php

namespace App\Repositories\Core\Eloquent;

use App\Models\Product;
use App\Repositories\Core\BaseEloquentRepository;
use App\Repositories\Contracts\ProductRepositoryInterface;

class EloquentProductRepository extends BaseEloquentRepository implements ProductRepositoryInterface
{
    public function entity()
    {
        return Product::class;
    }

    public function search(array $data)
    {
        return $this->entity
                    ->with('category')
                    ->where(function($query) use ($data){
                        if($data["name"])
                            $query->where('name', 'LIKE', "%".$data["name"]."%");

                        if($data["price"])
                            $query->orWhere('price', "".$data["price"]."");

                        if($data["description"])
                            $query->orWhere('description', 'LIKE', "%".$data["description"]."%");

                        if($data["category_id"])
                            $query->orWhere('category_id', $data["category_id"]);
                    })
                    ->paginate();
    }
}
