<?php

namespace App\Repositories\Core\Eloquent;

use App\Repositories\Core\BaseEloquentRepository;
use App\Repositories\Contracts\ChartsRepositoryInterface;

class EloquentChartRepository extends BaseEloquentRepository implements ChartsRepositoryInterface
{
    public function entity()
    {
        //return Chart::class;
    }
}
