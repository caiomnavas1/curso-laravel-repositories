<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateProductFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $id = $this->segment(3);

        return [
            'name'          => "required|min:3|max:100|unique:products,name,{$id},id",
            'url'           => "required|min:3|max:100|unique:products,url,{$id},id",
            'description'   => "max:9000",
            'category_id'      => "required|exists:categories,id"
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Campo Nome é obrigatório!',
            'name.min' => 'Campo Nome deve conter no mínimo 3 caracteres!',
            'name.max' => 'Campo Nome deve conter no máximo 100 caracteres!',
            'name.unique' => 'O Nome informado já existe!',
            'url.required' => 'Campo URL é obrigatório!',
            'url.min' => 'Campo URL deve conter no mínimo 3 caracteres!',
            'url.max' => 'Campo URL deve conter no máximo 100 caracteres!',
            'url.unique' => 'A URL informada já existe!',
            'description.max' => 'Campo Descrição deve conter no máximo 2.000 caracteres!',
            'category_id.required' => 'Campo Categoria é obrigatório!',
        ];
    }
}
