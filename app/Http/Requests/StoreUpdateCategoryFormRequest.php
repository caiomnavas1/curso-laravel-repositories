<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateCategoryFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $id = $this->segment(3);

        return [
            'title'         => "required|min:3|max:60|unique:categories,title,{$id},id",
//            'url'           => "required|min:3|max:60|unique:categories,url,{$id},id",
            'description'   => "max:2000"
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Campo Título é obrigatório!',
            'title.min' => 'Campo Título deve conter no mínimo 3 caracteres!',
            'title.max' => 'Campo Título deve conter no máximo 60 caracteres!',
            'title.unique' => 'O Título informado já existe!',
//            'url.required' => 'Campo URL é obrigatório!',
//            'url.min' => 'Campo URL deve conter no mínimo 3 caracteres!',
//            'url.max' => 'Campo URL deve conter no máximo 60 caracteres!',
//            'url.unique' => 'A URL informada já existe!',
            'description.max' => 'Campo Descrição deve conter no máximo 2.000 caracteres!',
        ];
    }
}
