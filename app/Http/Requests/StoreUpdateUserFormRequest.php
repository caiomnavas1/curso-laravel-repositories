<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rules\Password;
use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateUserFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $id = $this->segment(3);

        $rules = [
            'name'     => 'required|min:5|max:150',
            'email'    => "required|email|unique:users,email,{$id},id"
        ];

        if($this->isMethod('PUT'))
            $rules['password'] = '';
        else
            $rules['password'] = ['required', 'confirmed', Password::min(8)->letters()->numbers()];

        return $rules;
    }

    public function messages()
    {
        return [
            'password.numbers'  => 'senha com números',
        ];
    }
}
