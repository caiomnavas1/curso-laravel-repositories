@extends('adminlte::page')

@section('title', "Detalhes do Usuário: {$user->title}")

@section('content_header')
    <h1>Detalhes do Usuário: {{$user->title}}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('users.index') }}">Usuários</a></li>
        <li class="breadcrumb-item active">Detalhes do Usuário</li>
    </ol>
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <p>
                                <strong>ID:</strong> {{ $user->id }}
                            </p>
                            <p>
                                <strong>Título:</strong> {{ $user->name }}
                            </p>
                            <p>
                                <strong>URL:</strong> {{ $user->email }}
                            </p>
                            <hr>
                                {{ Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) }}
                                    {{ Form::submit('Deletar', ['class' => 'btn btn-sm btn-danger']) }}
                                {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
