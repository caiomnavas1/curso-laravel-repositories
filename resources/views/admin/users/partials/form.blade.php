<div class="form-group">
    {{ Form::text('name', null, ['placeholder' => 'Nome', 'class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::email('email', null, ['placeholder' => 'E-mail', 'class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::password('password', ['placeholder' => 'Senha', 'class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::password('password_confirmation', ['placeholder' => 'Confirme a senha', 'class' => 'form-control']) }}
</div>
<div class="form-group">
    <button type="submit" class="btn btn-primary">
        {{ (!isset($user->id)) ? 'Cadastrar' : 'Editar' }}
    </button>
</div>
