@extends('adminlte::page')

@section('title', 'Listagem de Usuários')

@section('content_header')
    <h1>Usuários</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin') }}">Dashboard</a></li>
        <li class="breadcrumb-item active">Usuários</li>
    </ol>
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <a href="{{ route('users.create') }}" class="btn btn-success">Novo Usuário</a>
                            <hr>
                            {{ Form::open(['route' => 'users.search', 'class' => 'form-inline' ]) }}
                            {{ Form::text('name', request()->name, ['placeholder' => 'Nome', 'class' => 'form-control']) }}
                            {{ Form::text('email', request()->email, ['placeholder' => 'E-mail', 'class' => 'form-control ml-1']) }}
                            {{ Form::submit('Pesquisar', ['class' => 'btn btn-primary ml-1']) }}
                            <a href="{{ route('users.index') }}" class="btn btn-danger ml-1">Limpar</a>
                            {{ Form::close() }}
                            <hr>
                            @include('admin.includes.alerts')
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="row">Nome</th>
                                    <th scope="row">E-mail</th>
                                    <th scope="row" class="text-center">Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('users.edit', $user->id) }}"
                                               class="btn btn-sm btn-warning">Editar</a>
                                            <a href="{{ route('users.show', $user->id) }}"
                                               class="btn btn-sm btn-primary">Detalhes</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="card-footer clearfix">
                            @if(isset($filters))
                                {!! $users->appends($filters)->links('vendor.pagination.bootstrap-5') !!}
                            @else
                                {!! $users->links('vendor.pagination.bootstrap-5') !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
