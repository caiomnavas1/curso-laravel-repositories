@if($errors->any())
    <div class="alert alert-warning mb-1">
        <ul class="mb-0">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session('success'))
    <div class="alert alert-success mb-2">
        {{ session('success') }}
    </div>
@endif

@if(session('danger'))
    <div class="alert alert-danger mb-2">
        {{ session('danger') }}
    </div>
@endif
