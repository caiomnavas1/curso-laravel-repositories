@extends('adminlte::page')

@section('title', "Detalhes da Categoria: {$category->title}")

@section('content_header')
    <h1>Detalhes da Categoria: {{$category->title}}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('categories.index') }}">Categorias</a></li>
        <li class="breadcrumb-item active">Detalhes da Categoria</li>
    </ol>
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <p>
                                <strong>ID:</strong> {{ $category->id }}
                            </p>
                            <p>
                                <strong>Título:</strong> {{ $category->title }}
                            </p>
                            <p>
                                <strong>URL:</strong> {{ $category->url }}
                            </p>
                            <p>
                                <strong>Descrição:</strong><br>
                                {{ $category->description }}
                            </p>
                            <hr>
                            <form action="{{ route('categories.destroy', $category->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm btn-danger">Deletar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
