@extends('adminlte::page')

@section('title', 'Listagem de Categorias')

@section('content_header')
    <h1>Categorias</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin') }}">Dashboard</a></li>
        <li class="breadcrumb-item active">Categorias</li>
    </ol>
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <a href="{{ route('categories.create') }}" class="btn btn-success">Nova Categoria</a>
                            <hr>
                            <form action="{{ route('categories.search') }}" method="post" class="form form-inline">
                                @csrf
                                <input type="text" name="title" id="title" class="form-control mr-1"
                                       value="{{ request()->title }}" placeholder="Título">
                                <input type="text" name="url" id="url" class="form-control mr-1"
                                       value="{{ request()->url }}" placeholder="URL">
                                <input type="text" name="description" id="description" class="form-control mr-1"
                                       value="{{ request()->description }}" placeholder="Descrição">
                                <button type="submit" class="btn btn-primary mr-1">Pesquisar</button>
                                <a href="{{ route('categories.index') }}" class="btn btn-danger">Limpar</a>
                            </form>
                            <hr>
                            @if(request()->search)
                                <p>Resultados para a pesquisa: <strong>{{ request()->search }}</strong></p>
                            @endif
                            @include('admin.includes.alerts')
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="row">#</th>
                                    <th scope="row">Título</th>
                                    <th scope="row">URL</th>
                                    <th scope="row" class="text-center">Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($categories as $category)
                                    <tr>
                                        <td>{{ $category->id }}</td>
                                        <td>{{ $category->title }}</td>
                                        <td>{{ $category->url }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('categories.edit', $category->id) }}"
                                               class="btn btn-sm btn-warning">Editar</a>
                                            <a href="{{ route('categories.show', $category->id) }}"
                                               class="btn btn-sm btn-primary">Detalhes</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="card-footer clearfix">
                            @if(isset($data))
                                {!! $categories->appends($data)->links('vendor.pagination.bootstrap-5') !!}
                            @else
                                {!! $categories->links('vendor.pagination.bootstrap-5') !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
