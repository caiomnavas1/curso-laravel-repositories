@csrf
<div class="form-group">
    <input type="text" name="title" id="title" placeholder="Título" class="form-control" value="{{ $category->title ?? old('title') }}">
</div>
<div class="form-group">
    <textarea name="description" id="description" cols="30" rows="10" placeholder="Descrição" class="form-control"> {{ $category->description ?? old('description') }}</textarea>
</div>
<div class="form-group">
    <button type="submit" class="btn btn-primary">
        {{ (!isset($category->id)) ? 'Cadastrar' : 'Editar' }}
    </button>
</div>
