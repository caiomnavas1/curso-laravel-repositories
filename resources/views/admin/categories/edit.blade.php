@extends('adminlte::page')

@section('title', "Editar Categoria: {$category->title}")

@section('content_header')
    <h1>Editar Categoria: {{$category->title}}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('categories.index') }}">Categorias</a></li>
        <li class="breadcrumb-item active">Editar Categoria</li>
    </ol>
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            @include('admin.includes.alerts')
                            <form action="{{ route('categories.update', $category->id) }}" method="post">
                                @method('PUT')
                                @include('admin.categories.partials.form')
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
