<div class="form-group">
    {{ Form::text('name', null, ['placeholder' => 'Nome', 'class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::text('url', null, ['placeholder' => 'Url', 'class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::text('price', null, ['placeholder' => 'Valor', 'class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::select('category_id', $categories, null, ['placeholder' => 'Selecione uma categoria', 'class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::textarea('description', null, ['placeholder' => 'Descrição', 'class' => 'form-control']) }}
</div>
<div class="form-group">
    <button type="submit" class="btn btn-primary">
        {{ (!isset($product->id)) ? 'Cadastrar' : 'Editar' }}
    </button>
</div>
