@extends('adminlte::page')

@section('title', "Detalhes do Produto: {$product->title}")

@section('content_header')
    <h1>Detalhes do Produto: {{$product->title}}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('products.index') }}">Produtos</a></li>
        <li class="breadcrumb-item active">Detalhes do Produto</li>
    </ol>
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <p>
                                <strong>ID:</strong> {{ $product->id }}
                            </p>
                            <p>
                                <strong>Título:</strong> {{ $product->name }}
                            </p>
                            <p>
                                <strong>URL:</strong> {{ $product->url }}
                            </p>
                            <p>
                                <strong>Categoria:</strong> {{ $product->category->title }}
                            </p>
                            <p>
                                <strong>Descrição:</strong><br>
                                {{ $product->description }}
                            </p>
                            <hr>
                                {{ Form::open(['route' => ['products.destroy', $product->id], 'method' => 'delete']) }}
                                    {{ Form::submit('Deletar', ['class' => 'btn btn-sm btn-danger']) }}
                                {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
