@extends('adminlte::page')

@section('title', 'Listagem de Produtos')

@section('content_header')
    <h1>Produtos</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin') }}">Dashboard</a></li>
        <li class="breadcrumb-item active">Produtos</li>
    </ol>
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <a href="{{ route('products.create') }}" class="btn btn-success">Novo Produto</a>
                            <hr>
                                {{ Form::open(['route' => 'products.search', 'class' => 'form-inline' ]) }}
                                    {{ Form::text('name', request()->name, ['placeholder' => 'Nome', 'class' => 'form-control']) }}
                                    {{ Form::text('price', request()->price, ['placeholder' => 'Preço', 'class' => 'form-control ml-1']) }}
                                    {{ Form::text('description', request()->description, ['placeholder' => 'Descrição', 'class' => 'form-control ml-1']) }}
                                    {{ Form::select('category_id', $categories, request()->category_id, ['placeholder' => 'Selecione uma categoria', 'class' => 'form-control ml-1']) }}
                                    {{ Form::submit('Pesquisar', ['class' => 'btn btn-primary ml-1']) }}
                                    <a href="{{ route('products.index') }}" class="btn btn-danger ml-1">Limpar</a>
                                {{ Form::close() }}
                            <hr>
                            @include('admin.includes.alerts')
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="row">Título</th>
                                    <th scope="row">Categoria</th>
                                    <th scope="row">Preço</th>
                                    <th scope="row" class="text-center">Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->category->title }}</td>
                                        <td>R$ {{ $product->price }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('products.edit', $product->id) }}"
                                               class="btn btn-sm btn-warning">Editar</a>
                                            <a href="{{ route('products.show', $product->id) }}"
                                               class="btn btn-sm btn-primary">Detalhes</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="card-footer clearfix">
                            @if(isset($filters))
                                {!! $products->appends($filters)->links('vendor.pagination.bootstrap-5') !!}
                            @else
                                {!! $products->links('vendor.pagination.bootstrap-5') !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
