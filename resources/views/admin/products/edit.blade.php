@extends('adminlte::page')

@section('title', "Editar Produto: {$product->title}")

@section('content_header')
    <h1>Editar Produto: {{$product->title}}</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ route('products.index') }}">Produtos</a></li>
        <li class="breadcrumb-item active">Editar Produto</li>
    </ol>
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            @include('admin.includes.alerts')
                            <!--<form action="{{ route('products.update', $product->id) }}" method="post">
                                @method('PUT')
                                @include('admin.products.partials.form')
                            </form>-->
                            {{ Form::model($product, ['route' => ['products.update', $product->id]]) }}
                                @include('admin.products.partials.form')
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
